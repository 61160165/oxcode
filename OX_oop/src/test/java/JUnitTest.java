
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import OOP.player;
import OOP.table;

public class JUnitTest {
    
    public JUnitTest() {
    }

    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    @Test
    public void testWin(){
        player o =new player();
        o.setPlayer('O');
        assertEquals(0,o.getWin());
    }
    public void testPlayer(){
        player o =new player();
        o.setPlayer('O');
        assertEquals('O',o.getPlayer());
    }
    public void testCol(){
        table t = new table();
        t.setRowCol(0, 0, 'x');
        t.setRowCol(1, 0, 'x');
        t.setRowCol(2, 0, 'x');
        assertEquals(true,t.checkColunm());
    }
    public void testRow(){
        table t = new table();
        t.setRowCol(0, 0, 'x');
        t.setRowCol(0, 1, 'x');
        t.setRowCol(0, 2, 'x');
        assertEquals(true,t.checkRow());
    }
    public void testVert(){
        table t = new table();
        t.setRowCol(0, 0, 'O');
        t.setRowCol(1, 1, 'O');
        t.setRowCol(2, 2, 'O');
        assertEquals(true,t.checkVertical());
    }
    public void testLose(){
        player o =new player();
        o.setPlayer('O');
        assertEquals(0,o.getLose());
    }
    public void testDraw(){
        player o =new player();
        o.setPlayer('O');
        o.Draw();
        assertEquals(1,o.getDraw());
    }
    public void testInput(){
        table t = new table();
        assertEquals(true,t.setRowCol(0, 0, 'X'));
    }
    public void testInputFail(){
        table t = new table();
        assertEquals(false,t.setRowCol(12, 0, 'X'));
    }
    public void testScore(){
        player o =new player();
        o.setPlayer('O');
        o.Draw();
        o.Win();
        String score = o.getScore();
        assertEquals(score,o.getScore());
    }
    public void testCheckWin(){
        table t = new table();
        assertEquals(false,t.checkWin());
    }
}
