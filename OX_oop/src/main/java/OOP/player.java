package OOP;
public class player {
    protected char player;
     int win,lose,draw;

    public player() {
        this.player=0;
        this.win=0;
        this.lose=0;
        this.draw=0;
    }

    player(char c) {
        this.player = c;
    }
    public void setPlayer(char n){
        this.player = n;
    }
    public char getPlayer(){
        return player;
    }
    public void Win(){
        this.win+=1;
    }
    public int getWin(){
        return this.win;
    }
    public void Lose(){
        this.lose+=1;
    }
    public int getLose(){
        return this.lose;
    }
    public void Draw(){
       this.draw+=1;
    } 
    public int getDraw(){
        return this.draw;
    }
    public String getScore(){
        return "Player "+player+" Win: "+win+" Lose: "+lose+" Draw: "+draw;
    }
}
