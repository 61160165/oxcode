package OOP;
public class table {

    private char[][] table = new char[3][3];
    private player player = new player('O');
    private player winner = new player('-');
    int lastCol;
    int lastRow;
    player first;
    player second;
    int countTurn=1;
    public table(player first, player second) {
        this.first = first;
        this.second = second;
        for (int i = 0; i < this.table.length; i++) {
            for (int j = 0; j < this.table[i].length; j++) {
                this.table[i][j] = '-';
            }
        }
    }

    public void showTable() {
        System.out.println("--OXGame--");
        System.out.println("  0 1 2");
        for (int i = 0; i < this.table.length; i++) {
            System.out.print(i + " ");
            for (int j = 0; j < this.table[i].length; j++) {
                System.out.print(this.table[i][j] + " ");
            }
            System.out.println();
        }
    }

    public char[][] getData() {
        return table;
    }

    public boolean setRowCol(int r, int c) {
        try {
            --r;
            --c;
            if (table[r][c] == '-') {
                table[r][c] = player.getPlayer();
                this.table[r][c] = player.getPlayer();
                lastCol = c;
                lastRow = r;
                return true;
            } else {
                System.out.println("You can't put in this position, Please try again.");
                return false;
            }
        } catch (Exception e) {
            System.out.println("This position out of bounds, Please try Again.");
            return false;
        }
    }

    public char switchTurn() {
        countTurn++;
        if (player.getPlayer() == 'O') {
            player.setPlayer('X');
            return player.getPlayer();
        } else {
            player.setPlayer('O');
            return player.getPlayer();
        }
    }

    public player getCurrentPlayer() {
        return player;
    }

     public boolean checkWin() {
        if(checkColunm() || checkRow() || checkX1() || checkX2()) {
            this.winner.setPlayer(player.getPlayer());
            updateStat();
            return true;
        }
        if(countTurn==9) {
            updateStat();
            return true;
        }
        return false;
    }
    public boolean checkRow() {
        for (int col = 0; col < this.table[lastRow].length; col++) {
            if (this.table[lastRow][col] != player.getPlayer()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkColunm() {
        for (int row = 0; row < this.table.length; row++) {
            if (this.table[row][lastCol] != player.getPlayer()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX1() {
        for (int i = 0; i < this.table.length; i++) {
            if (this.table[i][i] != player.getPlayer()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX2() {
        for (int i = 0; i < this.table.length; i++) {
            if (this.table[i][this.table.length - i - 1] != player.getPlayer()) {
                return false;
            }
        }
        return true;
    }

    public void updateStat() {
        if (this.first.getPlayer() == this.winner.getPlayer()) {
            this.first.Win();
            this.second.Lose();
        } else if (this.second.getPlayer() == this.winner.getPlayer()) {
            this.second.Win();
            this.first.Lose();
        } else {
            this.second.Draw();
            this.first.Draw();
        }
    }

    public player getWinner() {
        return winner;
    }
}
