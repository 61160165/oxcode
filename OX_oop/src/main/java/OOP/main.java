package OOP;
import java.util.Scanner;
public class main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        gameControl OX = new gameControl();
        while (true) {
            try {
                System.out.println("Menu OX Game");
                System.out.println(" 1.Play OX game");
                System.out.println(" 2.Show score player");
                System.out.println(" 3.Exit program");
                System.out.println("Please enter menu :");
                int menu = input.nextInt();
                System.out.println("----------");
                if (menu == 1) {
                    OX = new gameControl();
                    OX.startGame();
                }else if (menu == 2) {
                    OX.showScore();
                }else if (menu == 3) {
                    break;
                }else{
                    System.out.println("Please try again.");
                }
            } catch (Exception e) {
                 System.out.println("Please try again.");
            }
        }
        System.out.println("Thank for use this program.");
        System.out.println("Have a nice day, Bye.");
    }
}
