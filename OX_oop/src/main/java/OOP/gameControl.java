package OOP;
import java.util.Scanner;
public class gameControl{
    protected static int countTurn = 0;
    protected static player p1;
    protected static player p2;
    private table t;
    protected static char playerTurn = 0;
    
    public gameControl() {
       p1=new player('O');
       p2=new player('X');
       t = new table(p1,p2);
    }
    public void startGame(){
        t.showTable();
        showTurn();
        inputRowCol();
    }
    public void showTurn(){
        playerTurn = t.switchTurn();
        System.out.println("TURN "+countTurn+" PLAYER "+playerTurn);
    }
    
    public void inputRowCol(){
        Scanner input = new Scanner(System.in);
        System.out.println("PLEASE INPUT ROW AND COLUMN : ");
        int row = input.nextInt();
        int col = input.nextInt();
        if (t.setRowCol(row, col)) {
            showWin();
        } else {
            inputRowCol();
        }
    }
    public void showEnd(){
        if (playerTurn==p1.getPlayer()) {
            p1.Win();
            p2.Lose();
        }if (playerTurn==p2.getPlayer()) {
            p2.Win();
            p1.Lose();
        }
        System.out.println("--GameEnd--");
    }
    public void showWin(){
        if (countTurn==9) {
            p1.Draw();
            p2.Draw();
            System.out.println("----Draw----");
            showEnd();
        }else if (t.checkWin()) {
            showEnd();
        } else {
            startGame();
        }
    }
    public void showScore(){
       System.out.println(p1.getScore());
       System.out.println(p2.getScore());
    }
    

}
 