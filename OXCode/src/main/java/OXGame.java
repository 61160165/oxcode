import java.util.Scanner;
public class OXGame {
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char player = ' ';
    static int count = 0;
    private static void showTable(){
        System.out.println("  0 1 2");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i+" ");
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j]+" ");
            }
            System.out.println();
        }
    }
    private static void switchTurn(){
        count++;
        if(count%2==1){
            player='O'; 
        }else{
            player='X';
        }
        System.out.println("Turn "+count+" PLAYER "+player);
    }
    
    private static void startGame(){
        showTable();
        switchTurn();
        Input();
    } 
    
    private static void Input(){
        Scanner input = new Scanner(System.in);
        try { System.out.print("PLEASE INPUT ROW and COLUMN : ");
            int row = input.nextInt();
            int col = input.nextInt();
            if (table[row][col]=='-') {
                table[row][col] = player;
                System.out.println("----------");
                CheckWin();
            } else {
                System.out.println("You can't put it in, Please try again.");
                System.out.println("----------");
                Input();
            }
        } catch (Exception e) {
            System.out.println("This position out of bounds, Please try Again.");
            System.out.println("----------");
            Input();
        }
    }
  
    private static void CheckWin(){
        if (checkVertical()){
            showTable();
            System.out.println("In turn "+count+" PLAYER "+player+" with Vertical");
        }else if (checkRow()){
            showTable();
            System.out.println("In turn "+count+" PLAYER "+player+" with Row");  
        }else if (checkColunm()){
            showTable();
            System.out.println("In turn "+count+" PLAYER "+player+" with Column");
        }else if (checkDraw()) {
            showTable();
            System.out.println("----Draw----");
        }else {
            startGame();
        }
    }
    private static boolean checkVertical(){
        if (((table[0][0]==table[1][1]&&table[0][0]==table[2][2]&&table[1][1]==table[2][2])&&(table[0][0]!='-'&&table[1][1]!='-'&&table[2][2]!='-'))||
            ((table[2][0]==table[1][1]&&table[2][0]==table[0][2]&&table[1][1]==table[0][2])&&(table[2][0]!='-'&&table[1][1]!='-'&&table[0][2]!='-'))){
            return true;
        }else{
            return false;
        }
    }
    private static boolean checkRow(){
        if ((table[0][0]==table[0][1]&&table[0][0]==table[0][2]&&table[0][1]==table[0][2])&&(table[0][0]!='-'&&table[0][1]!='-'&&table[0][2]!='-')||
            (table[1][0]==table[1][1]&&table[1][0]==table[1][2]&&table[1][1]==table[1][2])&&(table[1][0]!='-'&&table[1][1]!='-'&&table[1][2]!='-')||
            (table[2][0]==table[2][1]&&table[2][0]==table[2][2]&&table[2][0]==table[2][2])&&(table[2][0]!='-'&&table[2][1]!='-'&&table[2][2]!='-')){
            return true;
        }else{
            return false;
        }
    }
    private static boolean checkColunm(){
        if ((table[0][0]==table[1][0]&&table[0][0]==table[2][0]&&table[1][0]==table[2][0]&&table[0][0]!='-'&&table[1][0]!='-'&&table[2][0]!='-')||
            (table[0][1]==table[1][1]&&table[1][2]==table[2][1]&&table[1][1]==table[2][1]&&table[0][1]!='-'&&table[1][1]!='-'&&table[1][2]!='-')||
            (table[0][2]==table[1][2]&&table[0][2]==table[2][2]&&table[1][2]==table[2][2]&&table[0][2]!='-'&&table[1][2]!='-'&&table[2][2]!='-')){
            return true;
        }else{
            return false;
        }
    }
    private static boolean checkDraw(){
        if (count==9) {
            return true;
        } else {
            return false;
        }
    }
    public static void main(String[] args) {
        System.out.println("--OXGame--");
        startGame();
        System.out.println("--GameEnd--");
    }
}
